/*
Diseñe un programa que lea la temperatura en centígrados del día e
imprima el tipo de clima de acuerdo a la siguiente tabla. (Use condicional
múltiple).
 */
package climacontemp;

import java.util.Scanner;

/**
 *
 * @author juanm
 */
public class ClimaConTemp {

    static Scanner sc = new Scanner(System.in);
    static float tempFrio = 10;
    static float tempNublado = 20;
    static float tempCaluroso = 30;
    static boolean preguntarSig = true;
    static String instruc = "Ingrese la temperatura en grados centigrados, o una letra para salir";

    static float tempActual;

    public static void main(String[] args) {
        System.out.println(instruc);

        try {
            tempActual = sc.nextFloat();
        } catch (java.util.InputMismatchException err) {
            preguntarSig = false;
        }
        while (preguntarSig) {
            if (tempActual <= tempFrio) {
                System.out.println("El clima es frio");
            } else if (tempActual > tempFrio && tempActual <= tempNublado) {
                System.out.println("El clima es nublado");
            } else if (tempActual > tempNublado && tempActual <= tempCaluroso) {
                System.out.println("El clima es caluroso");
            } else if (tempActual > tempCaluroso) {
                System.out.println("El clima es tropical");
            }
            System.out.println("======================================");
            System.out.println(instruc);
            try {
                tempActual = sc.nextFloat();
            } catch (java.util.InputMismatchException err) {
                preguntarSig = false;
            }

        }
        System.out.println("Se a ingresado una letra, cerrando...");

    }

}
